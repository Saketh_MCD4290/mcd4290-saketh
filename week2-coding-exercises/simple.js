//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1() {
    let output = "" //empty output, fill this so that it can print onto the page.

    //Question 1 here 

    let oddValues = [];
    let evenValues = [];
    let arrayValues = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];

    for (let i = 0; i < arrayValues.length; i++) {
        if ((arrayValues[i] < 0) && (arrayValues[i] % 2 === 0)) {
            evenValues.push(arrayValues[i]);
        }
        else if ((arrayValues[i] > 0) && (arrayValues[i] % 2 !== 0)) {
            oddValues.push(arrayValues[i]);
        }
    }

    output += ("Positive Odd: " + oddValues.join(', ') + "\n");
    output += ("Negative Even: " + evenValues.join(', '));


    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2() {
    let output = ""

    //Question 2 here 

    let one = two = three = four = five = six = randomNumber = 0;

    for (let i = 1; i <= 60000; i++) {
        randomNumber = Math.floor((Math.random() * 6) + 1);

        if (randomNumber === 1) {
            one++;
        }
        else if (randomNumber === 2) {
            two++;
        }
        else if (randomNumber === 3) {
            three++;
        }
        else if (randomNumber === 4) {
            four++;
        }
        else if (randomNumber === 5) {
            five++;
        } else if (randomNumber === 6) {
            six++;
        }
    }


    output += ("Frequency of die rolls" + "\n");
    output += ("1: " + one + "\n");
    output += ("2: " + two + "\n");
    output += ("3: " + three + "\n");
    output += ("4: " + four + "\n");
    output += ("5: " + five + "\n");
    output += ("6: " + six + "\n");

    let outPutArea = document.getElementById("outputArea2")
    outPutArea.innerText = output
}

function question3() {
    let output = ""

    //Question 3 here

    let dieArray = [0, 0, 0, 0, 0, 0, 0];
    let randomNumber;

    for (let i = 1; i <= 60000; i++) {
        randomNumber = Math.floor((Math.random() * 6) + 1);
        dieArray[randomNumber]++;
    }

    output += ("Frequency of die rolls" + "\n");

    for (let j = 1; j <= 6; j++) {
        output += (j + ": " + dieArray[j] + "\n");
    }

    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output
}

function question4() {
    let output = ""

    //Question 4 here 

    var dieRolls = {
        Frequencies: { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0 },
        Total: 60000,
        Exceptions: ""
    };

    var randomNumber;

    for (let i = 1; i <= 60000; i++) {
        randomNumber = Math.floor((Math.random() * 6) + 1);
        dieRolls.Frequencies[randomNumber]++;
    }

    for (let j = 1; j <= 6; j++) {
        if (9900 > dieRolls.Frequencies[j] || dieRolls.Frequencies[j] > 10100 ) {
            dieRolls.Exceptions += j + " "
        }
    }

    output += ("Frequency of dice rolls" + "\n");
    output += ("Total rolls: " + dieRolls.Total + "\n")
    output += ("Frequencies: " + "\n")
    for (let prop in dieRolls.Frequencies) {
        output += (prop + ": " + dieRolls.Frequencies[prop] + "\n");
    }
    output += ("Exceptions: " + dieRolls.Exceptions)

    let outPutArea = document.getElementById("outputArea4")
    outPutArea.innerText = output
}

function question5() {
    let output = ""

    //Question 5 here 

    let person = {
        name: "Jane",
        income: 127050
    }

    let incomeTax = 0;

    if(0 <= person.income && person.income <= 18200){
        output += ("Jane’s income is: $" + person.income + ", and her tax owed is: $" + incomeTax);
    }

    else if(18201 <= person.income && person.income <= 37000){
        incomeTax = (person.income - 18200) * 0.19;
        output += ("Jane’s income is: $" + person.income + ", and her tax owed is: $" + incomeTax);
    }

    else if(37001 <= person.income && person.income <= 90000){
        incomeTax = (person.income - 37000) * 0.325 + 3572;
        output += ("Jane’s income is: $" + person.income + ", and her tax owed is: $" + incomeTax);
    }

    else if(90001 <= person.income && person.income <= 180000){
        incomeTax = (person.income - 90000) * 0.37 + 20797;
        output += ("Jane’s income is: $" + person.income + ", and her tax owed is: $" + incomeTax);
    }

    else if(person.income >= 180001){
        incomeTax = (person.income - 180000) * 0.45 + 54097;
        output += ("Jane’s income is: $" + person.income + ", and her tax owed is: $" + incomeTax);
    }

    else{
        output += ("Jane’s income is invalid.");
    }
    
    let outPutArea = document.getElementById("outputArea5")
    outPutArea.innerText = output
}