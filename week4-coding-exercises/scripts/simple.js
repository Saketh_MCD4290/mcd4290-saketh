function doIt() {
    let oddEvenRef = document.getElementById("oddEven");
    let answerRef = document.getElementById("answer");
    let number1 = Number(document.getElementById("number1").value);
    let number2 = Number(document.getElementById("number2").value);
    let number3 = Number(document.getElementById("number3").value);
    let answer = number1 + number2 + number3;

    if (answer >= 0) {
        answerRef.innerText = answer;
        answerRef.className = "positive";
    }
    else {
        answerRef.innerText = answer;
        answerRef.className = "negative";
    }

    if ((answer % 2 === 0)) {
        oddEvenRef.className = "even";
        oddEvenRef.innerText = " (even)";
    }

    else {
        oddEvenRef.className = "odd";
        oddEvenRef.innerText = " (odd)";
    }
}