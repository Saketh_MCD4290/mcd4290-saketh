/** 
 * main.js 
 * This file contains code that runs on load for index.html
 */
 "use strict";
 // TODO: Write the function displayLockers

 // Display all lockers using locker list data and output on index page
 
 function displayLockers(data){
     let output = "";

     // For each locker in locker array get all attributes

     let lockersArray = data.lockers;
     for(let i = 0; i < lockersArray.length; i++){
         output += '<div class="mdl-cell mdl-cell--4-col">';
         output += '<div class="mdl-card mdl-shadow--2dp locker" style="background-color:#' + lockersArray[i].color + '">';
         output += '<div class="mdl-card__title mdl-card--expand">';
         output += '<h2>' + lockersArray[i].id + '</h2>';
         if(!(lockersArray[i].label === "New Locker")){
             output += '<h4>&nbsp;' + lockersArray[i].label + '\'s locker</h4>';
         }
         else{
             output += '<h4>&nbsp;' + lockersArray[i].label + '</h4>';
         }
         output += '</div>';
         output += '<div class="mdl-card__actions mdl-card--border">';
         output += '<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="view('+ i +')">Open Locker</a>';
         output += '<div class="mdl-layout-spacer"></div>';
         
         // If locker is locked use closed lock material-icon 
         // Else use open lock material-icon
        
         if(lockersArray[i].locked){
             output += '<i class="material-icons">lock</i>';
         }
         else{
             output += '<i class="material-icons">lock_open</i>';
         }
         output += '</div></div></div>' 
     }

     // For each locker in locker array output and display all attributes

     let lockerDisplayRef = document.getElementById("lockerDisplay");
     lockerDisplayRef.innerHTML = output;
 }
 
 // TODO: Write the function addNewLocker
 
 // If not first locker generate an id based on incrementing the last locker id and set random color attribute
 // Else set locker set id to A001 and random color attribute
 // Update local storage and display new locker list data

 function addNewLocker(){
     if((lockers.count >= 1) && (lockers._lockers[0].id==="A001")){
         let lastIndex = lockers.count - 1 
         let lockerValue = Number(lockers.lockers[lastIndex].id.slice(1,4)) + 1;
         if(lockerValue >= 100){
             lockers.addLocker("A" + lockerValue);
             lastIndex = lockers.count - 1;
             lockers.lockers[lastIndex].label = "New Locker";
             lockers.lockers[lastIndex].color = Math.random().toString(16).substr(2,6);
         }
         else if(lockerValue >= 10){
             lockers.addLocker("A0" + lockerValue);
             lastIndex = lockers.count - 1;
             lockers.lockers[lastIndex].label = "New Locker";
             lockers.lockers[lastIndex].color = Math.random().toString(16).substr(2,6);
         }
         else if(lockerValue > 1){
             lockers.addLocker("A00" + lockerValue);
             lastIndex = lockers.count - 1;
             lockers.lockers[lastIndex].label = "New Locker";
             lockers.lockers[lastIndex].color = Math.random().toString(16).substr(2,6);
         }
     }
     else{
         lockers.addLocker("A001");
         lockers.lockers[0].label = "New Locker";
         lockers.lockers[0].color = Math.random().toString(16).substr(2,6);
     }
     updateLocalStorage(lockers);
     displayLockers(lockers);
 }
 
 // TODO: Write the function view

 // Stringify locker index and store in local storage using LOCKER_INDEX_KEY
 // Redirect user to view page
 
 function view(index){
     let indexDataJSON = JSON.stringify(index);
     localStorage.setItem(LOCKER_INDEX_KEY,indexDataJSON);
     window.location = "view.html";
 }
 
 // TODO: Write the code that will run on load here

 // Display all lockers in global locker list variable in index page on window load 
 
 window.addEventListener('load', (event) => {
     displayLockers(lockers);
 });