/** 
 * shared.js 
 * This file contains shared code that runs on both view.html and index.html
 */

 "use strict";
 // Constants used as KEYS for LocalStorage
 const LOCKER_INDEX_KEY = "selectedLockerIndex";
 const LOCKER_DATA_KEY = "lockerLocalData";
 // TODO: Write code to implement the Locker class
 
 // Create Locker class with all attributes and methods from class design.

 class Locker {
    constructor(id) {
        this._id = id;
        this._label = "";
        this._locked = false;
        this._pin = "";
        this._color = "3399ff";
        this._contents = "";
    }

    // This accessor should return locker id attribute from the locker class object.

    get id() {
        return this._id;
    }

    // This accessor should return locker label attribute from the locker class object.

    get label() {
        return this._label;
    }

    // This accessor should return locker locked attribute from the locker class object.

    get locked() {
        return this._locked;
    }

    // This accessor should return locker pin attribute from the locker class object.

    get pin() {
        return this._pin;
    }

    // This accessor should return locker color attribute from the locker class object.

    get color() {
        return this._color;
    }

    // This accessor should return locker contents attribute from the locker class object.
    
    get contents() {
        return this._contents;
    }

    // This mutator should change the locker label attribute from the locker class object if typeof string.

    set label(text) {
        if ((typeof (text) === "string")) {
            this._label = text;
        }
    }

    // This mutator should change the locker locked attribute from the locker class object if typeof boolean.

    set locked(state) {
        if ((typeof (state) === "boolean")) {
            this._locked = state;
        }
    }

    // This mutator should change the locker pin attribute from the locker class object if typeof string.

    set pin(pin) {
        if ((typeof (pin) === "string")) {
            this._pin = pin;
        }
    }

    // This mutator should change the locker color attribute from the locker class object if typeof string.

    set color(color) {
        if ((typeof (color) === "string")) {
            this._color = color;
        }
    }

    // This mutator should change the locker contents attribute from the locker class object if typeof string.

    set contents(text) {
        if ((typeof (text) === "string")) {
            this._contents = text;
        }
    }

    // This method should restore the locker data into a new locker class instance.

    fromData(newLockerObject) {
        this._id = newLockerObject._id;
        this._label = newLockerObject._label;
        this._locked = newLockerObject._locked;
        this._pin = newLockerObject._pin;
        this._color = newLockerObject._color;
        this._contents = newLockerObject._contents;
    }

}
  
 // TODO: Write code to implement the LockerList class

 // Create Locker List class with all attributes and methods from class design.
 
 class LockerList {
    constructor() {
        this._lockers = [];
    }

    // This accessor should return all lockers currently in the lockers array.

    get lockers() {
        return this._lockers;
    }

    // This accessor should return the number of lockers currently in the lockers array.

    get count() {
        return this._lockers.length;
    }

    // This method should create a new instance of Locker using the id provided and add it to the lockers currently in the lockers array.

    addLocker(id) {
        let newLocker = new Locker(id);
        this._lockers.push(newLocker);
    }

    // This method should return the Locker currently in the lockers array using the id provided.

    getLocker(index) {
        return this._lockers[index];
    }

    // This method should remove the Locker currently in the lockers array using the id provided.

    removeLocker(id) {
        for(let i = 0; i < this.count ; i++){
            if ((this.lockers[i].id === id)) {
                this._lockers.splice(i, 1);
            }
        }
    }
    
    // This method should restore the locker list data into a new locker list class instance.
    
        fromData(newLockerListObject) {
        for(let i = 0; i < newLockerListObject._lockers.length; i++) {
            let newLocker = new Locker();
            newLocker.fromData(newLockerListObject._lockers[i]);
            this._lockers.push(newLocker);
        }
    }

}
 
 // TODO: Write the function checkIfDataExistsLocalStorage
 
 // Check if data exists in local storage.
 // If data does exist does not contain null does not contain undefined does not contain blank string return true.
 // Else return false.

 function checkIfDataExistsLocalStorage(){
    let localDataJSON = localStorage.getItem(LOCKER_DATA_KEY);
    if(localDataJSON){
        return true;
    }
    else{
        return false;
    }
 }
 
 // TODO: Write the function updateLocalStorage

 // Stringify data and set local storage using LOCKER_DATA_KEY to update local storage.
 
 function updateLocalStorage(data){
    let localDataJSON = JSON.stringify(data);
    localStorage.setItem(LOCKER_DATA_KEY,localDataJSON);
 }
 
 // TODO: Write the function getDataLocalStorage

 // Parse data from local storage using LOCKER_DATA_KEY then return it.
 
 function getDataLocalStorage(){
    let lockers = new LockerList(); 
    let lockerDataJSON = localStorage.getItem(LOCKER_DATA_KEY);
    lockers.fromData(JSON.parse(lockerDataJSON));
    return lockers;
 }
 
 // Global LockerList instance variable
 let lockers = new LockerList();
 
 // TODO: Write the code that will run on load here

 // If data exists in local storage on window load then retrieve store it.
 // Else create and add a new instance of Locker and set id to A001 and random color attribute and update local storage.

window.addEventListener('load', (event) => {
    if (checkIfDataExistsLocalStorage()) {
        lockers = getDataLocalStorage();
    }
    else {
        lockers.addLocker("A001");
        lockers.lockers[0].label = "New Locker";
        lockers.lockers[0].color = Math.random().toString(16).substr(2,6);
        updateLocalStorage(lockers);
    }
});