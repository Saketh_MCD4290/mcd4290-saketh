/** 
 * view.js 
 * This file contains code that runs on load for view.html
 */

 "use strict";

 let currentLocker;
 // TODO: Write the function displayLockerInfo

 // Display Locker class data with all attributes and methods and color
 
 function displayLockerInfo(locker){
   document.getElementById("lockerId").innerText = locker.id;
   document.getElementById("lockerContents").value = locker.contents;
   document.getElementById("lockerLabel").value = locker.label;
   document.getElementById("lockerColor").value = locker.color.toUpperCase();
   document.getElementById("lockerColor").style.backgroundColor = "#" + locker.color.toUpperCase();
   document.getElementById("deleteLocker").addEventListener("click", deleteThisLocker);
 }
 
 // TODO: Write the function unlock

 // If locker locked prompt pin if matches pin value change locker locked to false and change pin to "" and display locker
 // Else redirect user to index page
 
 function unlock(locker){
   if(locker.locked){
     let pinValue = prompt("Enter the pin");
     if(locker.pin === pinValue){
       locker.locked = false;
       locker.pin = "";
     }
     else{
       window.location = "index.html";
     }
   }
   displayLockerInfo(locker);
 }
 
 // TODO: Write the function deleteThisLocker

 // If delete locker confirm remove locker update local storage alert and redirect user to index page
 
 function deleteThisLocker() {
   // Confirm with the user
   if (confirm("Confirm delete")) {
     // runs if user clicks 'OK'
     lockers.removeLocker(document.getElementById("lockerId").innerText);
     updateLocalStorage(lockers);
     alert("Locker deleted");
     window.location = "index.html";
   }
   else {
     // runs if user clicks 'Cancel'
   }
 
 }
 
 
 // TODO: Write the function lockLocker
 
 // If lock locker confirm prompt and confirm new pin set new locker label pin color contents color and update local storage alert and redirect user to index page
 // Else alert user incorrect value entered

 function lockLocker() {
   // Confirm with the user
   if (confirm("Confirm lock locker")) {
     // runs if user clicks 'OK'
     let pinValue = prompt("Enter new pin");
     let newPinValue = prompt("Confirm pin");
     
     if (pinValue === newPinValue) {
       currentLocker.label = document.getElementById("lockerLabel").value;
       currentLocker.locked = true;
       currentLocker.pin = pinValue;
       currentLocker.color = document.getElementById("lockerColor").value.toLowerCase();
       currentLocker.contents = document.getElementById("lockerContents").value;
       updateLocalStorage(lockers);
       alert("Locker Locked");
       window.location = "index.html";
     }
     else{
       alert("Incorrect value entered");
     }
   }
   else {
     // runs if user clicks 'Cancel'
   }
 }
 
 // TODO: Write the function closeLocker

 // If confirm close locker without locking set new locker label pin color contents color and update local storage alert and redirect user to index page
 
 function closeLocker() {
   // Confirm with the user
   if (confirm("Confirm close without locking")) {
     // runs if user clicks 'OK'
     currentLocker.label = document.getElementById("lockerLabel").value;
     currentLocker.locked = false;
     currentLocker.pin = "";
     currentLocker.color = document.getElementById("lockerColor").value.toLowerCase();
     currentLocker.contents = document.getElementById("lockerContents").value;
     updateLocalStorage(lockers);
     alert("Locker closed without locking");
     window.location = "index.html";
   }
   else {
     // runs if user clicks 'Cancel'
   }
 }
 
 // TODO: Write the code that will run on load here

 // Retrieve index from local storage and unlock current locker instance
 
 window.addEventListener('load', (event) => {
   
  // Retrieve stored index from local storage
 
   let localIndexValue = localStorage.getItem(LOCKER_INDEX_KEY);
   let index = Number(localIndexValue);
 
   // Using getLocker method retrieve current locker instance
 
   currentLocker = lockers.getLocker(index);
   unlock(currentLocker);
 });