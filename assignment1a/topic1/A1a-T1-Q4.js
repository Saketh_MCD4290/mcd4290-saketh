/* 
  If Day 1,21,31 suffix is st
  If Day 2,22 suffix is nd
  If Day 3, 23 suffix is rd
  Else If Day is integer in range, suffix is st
  Else Day suffix is null
*/

function daySuffix(day) {
  switch (day) {
    case 1:
    case 21:
    case 31:
      return day + ": " + day + "st";
    case 2:
    case 22:
      return day + ": " + day + "nd";
    case 3:
    case 23:
      return day + ": " + day + "rd";
    default:
      if ((Number.isInteger(day) && day < 31)) {
        return day + ": " + day + "th";
      }
      else {
        return day + ": null";
      }
  }
}

let output = "";
for (let i = 1; i <= 31; i++) {
  output += daySuffix(i) + "\n";
}
output += daySuffix("dog") + "\n";
output += daySuffix(-1) + "\n";
output += daySuffix(100) + "\n";
output += daySuffix("d0g") + "\n";
console.log(output)