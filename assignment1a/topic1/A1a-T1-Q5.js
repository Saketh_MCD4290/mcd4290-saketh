/* 
  Get current date
  Add days to current day from current date and set new current date
  Output new current date in console and return it
*/

function addDay(numDays) {
  let currentDate = new Date();
  currentDate.setDate(currentDate.getDate() + numDays);
  console.log(currentDate.toDateString());
  return currentDate.toDateString();
}