/*
  Loop through all values in marks array
  If mark value is within range and integer output true
  Else output false
*/

let marks = [12, 45, 67, 900, "dog", -1, true];

function checkMarkValid(mark) {
  let booleanValue = (0 <= mark && mark <= 100 && Number.isInteger(mark));
  return booleanValue;
}

for (let i = 0; i < mark.length; i++) {
  console.log(mark[i] + ": " + checkMarkValid(marks[i]));
}