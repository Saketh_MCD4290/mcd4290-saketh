/* 
  Calculate circle area using pi * radius * radius 
  Calculate circle circumference using 2 * pi * radius
  Calculate circle ratio using circle area / circle circumference
  While circle ratio is less than 30
  Output circle ratio and increment circle radius
  Else break 
*/

let radiusValue = 1;
let ratio = 0;
const MAX_RATIO = 30;

function calculateRatio(radius) {
  let circleArea = Math.PI * radius * radius;
  let circleCircumference = 2 * Math.PI * radius;
  let ratio = circleArea / circleCircumference;

  return ratio;
}

do {
  ratio = calculateRatio(radiusValue);

  if (ratio < MAX_RATIO) {
    console.log("Radius: " + radiusValue + ", ratio: " + ratio);
    radiusValue++;
  }
  else {
    break;
  }
}

while (ratio < MAX_RATIO);