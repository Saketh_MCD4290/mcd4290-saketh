/* 
  Convert fahrenheit to celcius
  Output celcius to 3 decimals
*/

function fahrenheitToCelsius(fahrenheit) {
  let celcius = (fahrenheit - 32) * (5 / 9);
  return celcius.toFixed(3);
}