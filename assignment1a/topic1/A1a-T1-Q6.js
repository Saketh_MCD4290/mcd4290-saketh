/* 
  Loop through student array objects
  If international student increment total count
  Return total count and output to console
*/

function searchInternationalStudents(dataArray) {
    let totalCount = 0;
    for (let i = 0; i < dataArray.length; i++) {
        if (dataArray[i].nationality !== "Australian") {
            totalCount++;
        }
    }
    return totalCount;
}

let students = [
    {
        id: 23978904,
        unit: ["ENG1003"],
        nationality: "Australian"
    },
    {
        id: 28976589,
        unit: ["ENG1003"],
        nationality: "American"
    },
    {
        id: 28951284,
        unit: ["ENG1003"],
        nationality: "Indian"
    },
    {
        id: 36789027,
        unit: ["ENG1003"],
        nationality: "Australian"
    },
    {
        id: 22345604,
        unit: ["ENG1003"],
        nationality: "Sri Lankan"
    }
];

console.log(searchInternationalStudents(students));