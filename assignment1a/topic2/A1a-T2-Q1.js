/* 
  This function takes all the values input by the user on the page in user object
  Using the ingredient costs calculate the total price for that order and input in user object
  Using a random number generator to obtain an order number
  Output user object values with order number
*/

function submitOrder() {
    let output = "";
    let user = {
        name: document.getElementById("nameValue").value,
        mobile: document.getElementById("mobileValue").value,
        meat: "",
        cheese: [],
        sauce: [],
        vegetables: [],
        spicyLevel: "",
        beverageChoice: "",
        total: 0
    }

    for (let i = 0; i < document.getElementsByName("meat").length; i++) {
        if (document.getElementsByName("meat")[i].checked) {
            user.meat = (document.getElementsByName("meat")[i].id);
            user.total += parseInt(document.getElementsByName("meat")[i].value);
        }
    }

    let condimentsArray = ["cheese", "sauce", "vegetables"];

    for(let i = 0; i < condimentsArray.length; i++){
        for (let j = 0; j < document.getElementsByName(condimentsArray[i]).length; j++) {
            if (document.getElementsByName(condimentsArray[i])[j].checked) {
                user[condimentsArray[i]].push(document.getElementsByName(condimentsArray[i])[j].id);
                user.total += parseFloat(document.getElementsByName(condimentsArray[i])[j].value);
            }
        }
    }

    user.spicyLevel = document.getElementById("spicyLevel").options[document.getElementById("spicyLevel").selectedIndex].id;
    user.total += parseFloat(document.getElementsByName("spicyLevel")[0].value);

    for (let i = 0; i < beverage.options.length; i++)
        if (beverage.options[i].value === beverageChoice.value) {
            user.beverageChoice = beverage.options[i].id;
            user.total += parseFloat(beverage.options[i].value.slice(beverage.options[i].value.length - 1, beverage.options[i].value.length))
        }

    output += user.name + ", your order number is " + (Math.floor(Math.random() * 300) + 1) + " and total cost is $" + user.total.toFixed(2) + ". <br />";
    output += "Your " + user.meat + " burger includes " + user.cheese.join(",") + "," + user.sauce.join(",") + "," + user.vegetables.join(",") + " and is " + user.spicyLevel + " spicy. Your drink is " + user.beverageChoice + ". <br />";
    output += "Enjoy!";
    document.getElementById("resultArea").innerHTML = output;
}