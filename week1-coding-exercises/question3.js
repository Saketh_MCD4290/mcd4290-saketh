let personObject = {firstName:"Kanye",lastName:"West", birthDate:"8 June 1977", annualIncome:150000000.00};

console.log(personObject.firstName + " " + personObject.lastName + " was born on " + personObject.birthDate + " and has an annual income of $" + personObject.annualIncome.toFixed());