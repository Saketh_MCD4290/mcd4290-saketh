// Assume the first element (at index 0) is the current minimum
// On each iteration compare the current element being processed with the current minimum
// If it’s less than the minimum then set it as the current minimum
// If it’s more than the minimum then set it as the current maximum
// Return array with the minimum and maximum values