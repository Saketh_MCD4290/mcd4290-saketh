let output = ""; //empty output, fill this so that it can print onto the page.

function flexible(fOperation, operand1, operand2) {
    var result = fOperation(operand1, operand2);
    return result;
}

function sum(num1, num2) {
    return (num1 + num2);
}

let multiplicationOperation = function (num1, num2) {
    return (num1 * num2);
}

output += flexible(sum, 3, 5) + "<br/>";
output += flexible(multiplicationOperation, 3, 5) + "<br/>";

document.getElementById("outputArea2").innerHTML = output //this line will fill the above element with your output.