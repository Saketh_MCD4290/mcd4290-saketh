let testObj = {
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
};

function objectToHTML(myObj) {
    let output = ""; //empty output, fill this so that it can print onto the page.
    for (let prop in myObj) {
        output += (prop + ": " + myObj[prop] + "\n");
    }
    return output;
}

document.getElementById("outputArea1").innerText = objectToHTML(testObj);