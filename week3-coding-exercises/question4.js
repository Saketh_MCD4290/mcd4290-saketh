function extremeValues(myArray) {
    let min = max = myArray[0];
    for (i = 1; i <= myArray.length; i++) {
        if (myArray[i] < min) {
            min = myArray[i];
        }
        else if (myArray[i] > max) {
            max = myArray[i];
        }
    }

    let outputArray = [min, max];

    return outputArray;
}

let values = [4, 3, 6, 12, 1, 3, 8];

let outputArray = extremeValues(values);

document.getElementById("outputArea3").innerText += outputArray[0] + "\n";
document.getElementById("outputArea3").innerText += outputArray[1] + "\n";